# Documentation

Documentation is one the most important aspects of a well-maintained (and maintainable) codebase.


# Why

Well-written documentation allows future users/maintainers to use, extend and debug your codebase.

It is also very useful for self-reference. As time passes our own code becomes more and more unreadable/unmaintainable for ourselves, especially if we haven't worked on it in a while.


# How / Where

Documentation should be committed to version control (i.e. git).

In your repos, there should be at least a `README` file. It should be written in an easy to read markup language, such as Markdown or plain text.

If your project is not only for your own use, and you plan on releasing it, you should have a `LICENSE` file.

If you plan on accepting merge requests, and especially if you're actively looking for contributors/maintainers, there should be a `CONTRIBUTING` file.

A `TODO` file, or a `TODO` section in your `README` is a good place to give an overview on the backlog.


# Writing a good README

A good `README` should give the user at least the following information:

-   What your project is
-   A short showcase/example to help users getting started
-   A link to more in-depth documentation
-   An overview over the features of your project
-   An overview over the roadmap / upcoming features


# When

When to start writing documentation?

Our instinct may tell us that we should focus on getting some code down before documenting it. We can always fix it later.

This is a potential pitfall. Later more todo tasks will fill the backlog, and many features later we may not have written a single line of documentation.


## Documentation Driven Development?

The best time to write documentation would be right before writing our tests in a TDD approach.

Start by writing down documentation.

Write the tests.

Write the code.

If needed, bring documentation up to date with the actual implementation.

Rinse and repeat.


# Different kind of documentation

Documentation can take different forms, and fill slightly different functions:

-   An **introduction** is a very short overview of what you can do with the product (i.e. "Getting Started")
-   A **tutorial** showcases some of the primary uses in more detail. It usually has several steps, and introduces the main concepts to the user
-   An **API reference** is usually autogenerated from the code (remember `docstrings`?). It will list all publicly available interfaces
-   **Developer documentation** is a more in-depth guide for potential contributors. It explains details and conventions used in the code, the design strategy for the project and possibly the git workflow and branching approach


# Sphinx / Readthedocs

[Sphinx](https://www.sphinx-doc.org/en/master/) is by far the most popular tool to autogenerate API reference from the code.

[Read The Docs](http://readthedocs.org/) is a free hosting for Sphinx documentation. Many popular python projects host their documentation there.


# Documenting Python Code

In Python you have both comments `#` and docstrings `"""`.

Docstrings describe modules, classes and functions.

Comments clarify the code, and are more like a programmer's note. They will only be visible by reading the source code, while docstrings can easily be extracted (i.e. with Sphinx).

A docstring for a function should convey the meaning of its arguments and return value.

[See PEP8 for a section about comments](https://peps.python.org/pep-0008/#comments)

[See PEP257 for more info about docstrings](https://peps.python.org/pep-0257/#specification)


# Wikis

When not having all the documentation in the repository, usually some kind of wiki system is used. One of the most popular ones is Confluence, by Atlassian.

GitHub, GitLab and other git web servers implement wikis builtin into the repos.


# Lab

Check out the subdirectory `document-this`, read and document the code.